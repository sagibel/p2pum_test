/*
 * TestUtils.hpp
 *
 *  Created on: Jun 2, 2013
 *      Author: user
 */

#ifndef TESTUTILS_HPP_
#define TESTUTILS_HPP_

#include "ConnectionManager.h"
#include "ConnectionHandler.h"
#include "EventListener.h"
#include "MessageListener.h"
#include "LogListener.h"
#include "P2PUM_Definitions.h"
#include "Network_Messaging/Message.h"
#include "Event.h"
#include "RxMessage.h"
#include "Connectable.h"

#include <boost/archive/text_iarchive.hpp>

namespace p2pum{

class EmptyHandler: public ConnectionHandler{

public:
	EmptyHandler(ConnectionManager_SPtr _cm){
		cm = _cm;
	}
	~EmptyHandler(){}

	void handleRead(char* data, ConnectionID conID, const error_code& code, size_t bytes_transferred, size_t bytes_expected){
		std::cout << "handle read invoked. bytes transferred: " << bytes_transferred << ", bytes expected: " << bytes_expected << ", error code: " << code.message() << std::endl;
		Connection_SPtr con = cm->getConnection(conID);
		Connectable_SPtr conn = boost::dynamic_pointer_cast<Connectable, Connection>(con);
		std::string archive_data(data);
		std::istringstream archive_stream(archive_data);
		boost::archive::text_iarchive archive(archive_stream);
		Message_SPtr message/* = MessageFactory::createMessage(type)*/;
		archive >> message;
		conn->readFromSocket();
	}

	void handleWrite(Message_SPtr message, ConnectionID conID, const error_code& code, size_t bytes_transferred, size_t bytes_expected){
		std::cout << "handle write invoked. bytes transferred: " << bytes_transferred << ", bytes expected: " << bytes_expected << ", error code: " << code.message() << std::endl;
	};

	void handleConnect(ConnectionID conID, const error_code& code){
		std::cout << "handle connect invoked. error code: " << code.message() << std::endl;
	}

	void handleAccept(Connection_SPtr con, const error_code& code){
		std::cout << "handle accept invoked. error code: " << code.message() << std::endl;
		Connectable_SPtr conn = boost::dynamic_pointer_cast<Connectable, Connection>(con);
		cm->addConnection(con);
		conn->readFromSocket();
	}

private:
	ConnectionManager_SPtr cm;
};

class EventListenerImpl: public EventListener
{
public:
	EventListenerImpl(){}
	virtual ~EventListenerImpl(){}

	EventReturnCode onEvent(const Event& event ){
		Event::Type type = event.getType();
		ConnectionID id = event.getConnectionID();
		std::cout << "event number: " << type << ", connection number: " << id << "." <<std::endl;
		if((type == Event::Connection_New) || (type == Event::Rx_New_Source)){
			return p2pum::Event_RC_Accept;
		}
		return p2pum::Event_RC_NoOp;
	}
};

class MessageListenerImpl: public MessageListener{
public:
	MessageListenerImpl(){}
	virtual ~MessageListenerImpl(){}

	void onMessage(const RxMessage& message ){
		const char* buf = message.getBuffer();
		MessageID id = message.getMessageID();
		std::cout << "message number: " << id << ", message content: " << buf << "." <<std::endl;
	}
};

class EventListenerOther: public EventListener
{
public:
	EventListenerOther(){}
	virtual ~EventListenerOther(){}

	EventReturnCode onEvent(const Event& event ){
		Event::Type type = event.getType();
		ConnectionID id = event.getConnectionID();
		std::cout << "event number: " << type << ", connection number: " << id << "." <<std::endl;
		if((type == Event::Connection_New) || (type == Event::Rx_New_Source)){
			if(type == Event::Connection_New){
				incomingC = event.getConnection();
			}
			if(type == Event::Rx_New_Source){
				incomingS = event.getStreamID();
			}
			return p2pum::Event_RC_Accept;
		}
		if(type == Event::Connection_Establish_Success){
			outgoingC = event.getConnection();
		}
		return p2pum::Event_RC_NoOp;
	}

	Connection_SPtr incomingC;
	Connection_SPtr outgoingC;
	StreamID incomingS;
};

class LogListenerImpl: public LogListener{
public:
	LogListenerImpl(){}
	virtual ~LogListenerImpl(){}

	void onLogMessage(LogLevel logLevel, uint64_t timeStamp,
			uint64_t threadId, std::string message){
	}
};

class MessageListenerChoking: public MessageListener{
public:
	MessageListenerChoking(){
		count = 0;
	}
	virtual ~MessageListenerChoking(){}

	void onMessage(const RxMessage& message ){
		if(count == 0){
			sleep(30); //30 sec, enough for queue to become full and heartbeat failure
			count++;
		}
		return;
	}

	int count;
};

}

#endif /* TESTUTILS_HPP_ */

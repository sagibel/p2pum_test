/*
 * MaintainerTest.cpp
 *
 *  Created on: Jun 4, 2013
 *      Author: user
 */

#include <boost/test/unit_test.hpp>

#include "TestUtils.hpp"

#include "ConnectionImpl.h"
#include "ServiceThread.h"
#include "Communicator.h"
#include "QueueManager.h"
#include "Maintainer.h"

//=== Core_suite ===
BOOST_AUTO_TEST_SUITE( Core_suite )

/**
 * Just say hello
 */
BOOST_AUTO_TEST_CASE( maintainer )
{
	using namespace p2pum;

	std::cout << "MaintainerTest started" << std::endl;

	IOService_SPtr service(new boost::asio::io_service());
	ServiceThread_SPtr serviceThread(new ServiceThread(service));
	serviceThread->start();
	ConnectionManager_SPtr cm(new ConnectionManager(PropertyMap()));
	QueueManager_SPtr qm(new QueueManager(PropertyMap()));
	Handler_SPtr handler(new EmptyHandler(cm));
	Communicator_SPtr communicator(new Communicator(new EventListenerImpl(), new LogListenerImpl(), qm, cm, handler));
	communicator->start();
	Maintainer_SPtr maintainer(new Maintainer(qm, cm));
	maintainer->start();
	ConnectionParams params;
	params.heartbeat_interval_milli = 1000000;//irrelevant
	params.heartbeat_timeout_milli = 2000;//2sec
	params.address = "127.0.0.1";
	params.connect_msg = "";
	params.context = 0;
	params.establish_timeout_milli = 100000;//not relevant now
	params.port = 5000;
	Connection_SPtr con1(new ConnectionImpl(service, params, handler));
	cm->addConnection(con1);
	cm->updateConnectionStatus(con1->getConnectionID(), Connection::Established);
	sleep(3); //let connection timeout
	BOOST_CHECK(!cm->hasIncoming());
	BOOST_CHECK(!cm->hasOutgoing());
	//also event should be printed
	params.heartbeat_timeout_milli = 2500; //2.5sec
	Connection_SPtr con2(new ConnectionImpl(service, params, handler));
	cm->addConnection(con2);
	cm->updateConnectionStatus(con2->getConnectionID(), Connection::Established);
	cm->updateConnectionStatus(con1->getConnectionID(), Connection::Established);
	sleep(1);
	cm->receiveHeartbeat(con1->getConnectionID(), NO_ID);//now con1 is 2 secs for ending and con2 is 1.5 secs
	sleep(3);//let them both run out. con 2 should run out before con1
	BOOST_CHECK(!cm->hasIncoming());
	BOOST_CHECK(!cm->hasOutgoing());

	maintainer->stop();
	maintainer->join();
	communicator->stop();
	communicator->join();
	serviceThread->stop();
	serviceThread->join();

	std::cout << "MaintainerTest finished successfully" << std::endl;

}

BOOST_AUTO_TEST_SUITE_END() // Core_suite

//EOF

/*
 * QueueManagerTest.cpp
 *
 *  Created on: Mar 31, 2013
 *      Author: sagibel
 */

#include <boost/test/unit_test.hpp>

#include "QueueManager.h"
#include "EventImpl.h"
#include "RxMessageImpl.h"
#include "ConnectionImpl.h"
#include "P2PUM_Definitions.h"
#include "ConnectionHandlerImpl.h"
#include "Listener.h"

#include <boost/asio.hpp>

//=== Core_suite ===
BOOST_AUTO_TEST_SUITE( Core_suite )

/**
 * Just say hello
 */
BOOST_AUTO_TEST_CASE( queue_manager )
{
	using namespace p2pum;

	std::cout << "QueueManagerTest started" << std::endl;

	IOService_SPtr service(new boost::asio::io_service);

	QueueManager_SPtr qm(new QueueManager(PropertyMap()));
	ConnectionManager_SPtr cm(new ConnectionManager(PropertyMap()));
	Listener_SPtr listener(new Listener(service, 5000));
	Handler_SPtr handler(new ConnectionHandlerImpl(cm, qm, listener));

	Connection_SPtr con(new ConnectionImpl(service, handler));

	char* c = "";

	Event_SPtr ev1(new EventImpl(Event::Connection_Closed, con, con->getConnectionID()));
	qm->pushEvent(ev1);
	Event_SPtr ev2(new EventImpl(Event::Connection_Ready, con, con->getConnectionID()));
	qm->pushEvent(ev2);
	Event_SPtr ev3(new EventImpl(Event::Connection_New, con, con->getConnectionID()));
	qm->pushEvent(ev3);
	RxMessage_SPtr m1(new RxMessageImpl(c, 0, 0, 0));
	qm->pushMessage(m1);
	RxMessage_SPtr m2(new RxMessageImpl(c, 0, 0, 0));
	qm->pushMessage(m2);
	RxMessage_SPtr m3(new RxMessageImpl(c, 0, 0, 0));
	qm->pushMessage(m3);
	BOOST_CHECK_EQUAL(ev1, qm->popEvent());
	BOOST_CHECK_EQUAL(ev2, qm->popEvent());
	BOOST_CHECK_EQUAL(ev3, qm->popEvent());
	BOOST_CHECK_EQUAL(m1, qm->popMessage());
	BOOST_CHECK_EQUAL(m2, qm->popMessage());
	BOOST_CHECK_EQUAL(m3, qm->popMessage());

	listener->close();

	std::cout << "QueueManagerTest finished successfully" << std::endl;
}

BOOST_AUTO_TEST_SUITE_END() // Core_suite

//EOF

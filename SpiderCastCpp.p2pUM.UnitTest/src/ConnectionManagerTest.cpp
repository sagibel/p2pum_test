/*
 * ConnectionManagerTest.cpp
 *
 *  Created on: Apr 1, 2013
 *      Author: sagibel
 */

#include <boost/test/unit_test.hpp>
#include <boost/asio.hpp>
#include <boost/shared_ptr.hpp>

#include "ConnectionParams.h"
#include "ConnectionManager.h"
#include "ConnectionImpl.h"
#include "Expirable.h"
#include "Refreshable.h"
#include "P2PUM_Definitions.h"
#include "ConnectionHandlerImpl.h"
#include "Listener.h"

#include <map>

//=== Core_suite ===
BOOST_AUTO_TEST_SUITE( Core_suite )

/**
 * Just say hello
 */
BOOST_AUTO_TEST_CASE( connection_manager )
{
	using namespace p2pum;

	std::cout << "ConnectionManagerTest started" << std::endl;

	IOService_SPtr service(new boost::asio::io_service);

	ConnectionManager_SPtr cm(new ConnectionManager(PropertyMap()));
	QueueManager_SPtr qm(new QueueManager(PropertyMap()));

	ConnectionParams params;
	params.heartbeat_interval_milli = 100000;//100sec
	params.heartbeat_timeout_milli = 1000000;//1000sec
	params.address = "127.0.0.1";
	params.connect_msg = "";
	params.context = 0;
	params.establish_timeout_milli = 100000;
	params.port = 5000;

	Listener_SPtr listener(new Listener(service, 5000));
	Handler_SPtr handler(new ConnectionHandlerImpl(cm, qm, listener));

	Connection_SPtr con1(new ConnectionImpl(service, params, handler));
	Connection_SPtr con2(new ConnectionImpl(service, params, handler));
	Connection_SPtr con3(new ConnectionImpl(service, params, handler));

	cm->addConnection(con1);
	cm->addConnection(con2);
	cm->addConnection(con3);

	cm->updateConnectionStatus(con1->getConnectionID(), Connection::Established);
	sleep(1); //so there will be difference in time
	cm->updateConnectionStatus(con2->getConnectionID(), Connection::Established);
	sleep(1); //so there will be difference in time
	cm->updateConnectionStatus(con3->getConnectionID(), Connection::Established);

	Connection_SPtr c1 = cm->getConnection(con1->getConnectionID());
	Connection_SPtr c2 = cm->getConnection(con2->getConnectionID());
	Connection_SPtr c3 = cm->getConnection(con3->getConnectionID());

	BOOST_CHECK_EQUAL(c1, con1);
	BOOST_CHECK_EQUAL(c2, con2);
	BOOST_CHECK_EQUAL(c3, con3);

	Expirable_SPtr con1exp = boost::dynamic_pointer_cast<Expirable, Connection>(con1);
	Expirable_SPtr con2exp = boost::dynamic_pointer_cast<Expirable, Connection>(con2);
	Expirable_SPtr con3exp = boost::dynamic_pointer_cast<Expirable, Connection>(con3);
	Refreshable_SPtr con1ref = boost::dynamic_pointer_cast<Refreshable, Connection>(con1);
	Refreshable_SPtr con2ref = boost::dynamic_pointer_cast<Refreshable, Connection>(con2);
	Refreshable_SPtr con3ref = boost::dynamic_pointer_cast<Refreshable, Connection>(con3);

	sleep(5); //5sec

	cm->receiveHeartbeat(con1->getConnectionID());

	cm->sendHeartbeat(con1->getConnectionID());

	Expirable_SPtr e1 = cm->getNextIncoming();
	Connection_SPtr exp1 = boost::dynamic_pointer_cast<Connection, Expirable>(e1);
	BOOST_CHECK_EQUAL(exp1, con2);

	Refreshable_SPtr r1 = cm->getNextOutgoing();
	Connection_SPtr ref1 = boost::dynamic_pointer_cast<Connection, Refreshable>(r1);
	BOOST_CHECK_EQUAL(ref1, con2);

	cm->updateConnectionStatus(con2->getConnectionID(), Connection::Closed);

	Expirable_SPtr e2 = cm->getNextIncoming();
	Connection_SPtr exp2 = boost::dynamic_pointer_cast<Connection, Expirable>(e2);
	BOOST_CHECK_EQUAL(exp2, con3);

	Refreshable_SPtr r2 = cm->getNextOutgoing();
	Connection_SPtr ref2 = boost::dynamic_pointer_cast<Connection, Refreshable>(r2);
	BOOST_CHECK_EQUAL(ref2, con3);

	cm->updateConnectionStatus(con1->getConnectionID(), Connection::Closed);
	cm->updateConnectionStatus(con3->getConnectionID(), Connection::Closed);

	listener->close();

	std::cout << "ConnectionManagerTest finished successfully" << std::endl;

}

BOOST_AUTO_TEST_SUITE_END() // Core_suite

//EOF

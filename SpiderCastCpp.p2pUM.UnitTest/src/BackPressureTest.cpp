/*
 * BackPressureTest.cpp
 *
 *  Created on: Jun 29, 2013
 *      Author: user
 */

#include <boost/test/unit_test.hpp>

#include "TestUtils.hpp"

#include "StreamRx.h"
#include "StreamTx.h"
#include "P2PUM.h"
#include "P2PUM_Definitions.h"
#include "Network_Messaging/TxMessageImpl.h"

//=== Core_suite ===
BOOST_AUTO_TEST_SUITE( Core_suite )

/**
 * Just say hello
 */
BOOST_AUTO_TEST_CASE( back_pressure )
{
	using namespace p2pum;

	std::cout << "BackPressureTest started" << std::endl;

	EventListenerOther* el = new EventListenerOther();
	PropertyMap map;
	map["max_events"] = "10";
	map["max_messages"] = "10";
	map["port"] = "5000";
	P2PUM_SPtr instance1 = P2PUM::createInstance(map, el, new LogListenerImpl());
	map["port"] = "5001";
	P2PUM_SPtr instance2 = P2PUM::createInstance(map, el, new LogListenerImpl());
	ConnectionParams params;
	params.heartbeat_interval_milli = 1000;//1sec
	params.heartbeat_timeout_milli = 5000;//5sec
	params.address = "127.0.0.1";
	params.connect_msg = "";
	params.context = 0;
	params.establish_timeout_milli = 3000; //3sec
	params.port = 5000;
	instance2->establishConnection(params);
	sleep(1); //let connection establish
	PropertyMap props;
	props["heartbeat_interval"] = "5000";//5sec
	props["heartbeat_timeout"] = "10000";//10sec
	StreamRx_SPtr streamRx1 = instance1->createStreamRx(props, new MessageListenerChoking());
	StreamTx_SPtr streamTx1 = instance2->createStreamTx(props, el->outgoingC);
	sleep(1); //let stream establish
	char* content = "hello";
	Message_SPtr message(new TxMessageImpl(streamTx1->getStreamID(), content, sizeof(content)));
	TxMessage* messageTx = dynamic_cast<TxMessageImpl*>(message.get());
	try{
		while(true){
			streamTx1->sendMessage(*messageTx); //endless loop sending messages,
			sleep(1);
		}
	}
	catch(P2PUMException e){
		std::cout << e.what() << std::endl;
	}
	catch(std::exception e){
		std::cout << e.what() << std::endl;
	}
	streamRx1->close();
	sleep(1);
	el->outgoingC->close();
	sleep(1); //let connection close gracefully
	instance2->close();
	instance1->close();

	std::cout << "BackPressureTest finished successfully" << std::endl;

}

BOOST_AUTO_TEST_SUITE_END() // Core_suite




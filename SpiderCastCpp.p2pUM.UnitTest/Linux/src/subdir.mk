################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/BackPressureTest.cpp \
../src/BasicConnectionTest.cpp \
../src/CommunicatorTest.cpp \
../src/ConnectionManagerTest.cpp \
../src/IntegrationTestBig.cpp \
../src/IntegrationTestSmall.cpp \
../src/MaintainerTest.cpp \
../src/QueueManagerTest.cpp \
../src/SerializationTest.cpp \
../src/Test1.cpp \
../src/UnitTestMain.cpp 

OBJS += \
./src/BackPressureTest.o \
./src/BasicConnectionTest.o \
./src/CommunicatorTest.o \
./src/ConnectionManagerTest.o \
./src/IntegrationTestBig.o \
./src/IntegrationTestSmall.o \
./src/MaintainerTest.o \
./src/QueueManagerTest.o \
./src/SerializationTest.o \
./src/Test1.o \
./src/UnitTestMain.o 

CPP_DEPS += \
./src/BackPressureTest.d \
./src/BasicConnectionTest.d \
./src/CommunicatorTest.d \
./src/ConnectionManagerTest.d \
./src/IntegrationTestBig.d \
./src/IntegrationTestSmall.d \
./src/MaintainerTest.d \
./src/QueueManagerTest.d \
./src/SerializationTest.d \
./src/Test1.d \
./src/UnitTestMain.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -DBOOST_THREAD_DYN_LINK -DBOOST_TEST_DYN_LINK -I"/home/user/git/p2pum/API/include" -I"/home/user/git/p2pum/Impl/Include" -I"/home/user/boost_1_49_0" -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


